from flask import Flask
app = Flask(__name__)

import gspread
from google.oauth2 import service_account
from datetime import datetime, timedelta
import os
from gcp_utils import generate_df_from_bigquery, generate_df_from_googlesheet, main_export_results, Prepare_results
from config import *
from MyProblem import MyProblem
from pymoo.algorithms.nsga3 import NSGA3
from pymoo.algorithms.nsga2 import NSGA2
from pymoo.factory import get_problem, get_reference_directions, get_sampling, get_crossover, get_mutation
from pymoo.optimize import minimize
from pymoo.model.problem import Problem
import numpy as np

@app.route("/")
def main(request):
    """Main function that loads the data, makes the forecasts, and exports the results"""
    print('Function running...')
    #time.sleep(60000)
    #timeout()
    
    # Get cooling load-------------------------------------------------------------------
    df=generate_df_from_googlesheet()

    # Load staging optimisation----------------------------------------------------------
    capacity_V0 = df.copy()
    vectorized_problem = MyProblem(capacity_V0)

    iniPop = np.random.random((200, vectorized_problem.n_var))


    ##NSG2
    ## create the algorithm object 
    algorithm = NSGA2( pop_size=300,sampling=iniPop)

    ## execute the optimization
    res_2 = minimize(vectorized_problem,
               algorithm,
               termination=('n_gen', 600),
               seed=1,verbose=False)

    #ref_dirs = get_reference_directions("das-dennis", 2, n_partitions=12)
    #algorithm = NSGA3(ref_dirs, pop_size=300)
    #res = minimize(vectorized_problem,
    #               algorithm,
    #               termination=('n_gen', 900),
    #               save_history=True,
    #               seed=1, verbose=False)

    # Prepare_results--------------------------------------------------------------------
    capacity_V=Prepare_results(capacity_V0,res_2)
    capacity_V=capacity_V.round(2)

    # Export results---------------------------------------------------------------------
    main_export_results(capacity_V)
    print('Results exported to google sheets')
    return 'Optimisation completed!'

def timeout(request):
    print('Function running...')
    #time.sleep(60000)

    # May not execute if function's timeout is <2 minutes
    print('Function completed!')
    return 'Function completed!'


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=int(os.environ.get("PORT", 8080)))