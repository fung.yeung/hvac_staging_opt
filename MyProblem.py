import numpy as np
import pandas as pd
import os
from pymoo.algorithms.nsga3 import NSGA3
from pymoo.factory import get_problem, get_reference_directions
from pymoo.optimize import minimize
from pymoo.model.problem import Problem
from config import *


class MyProblem(Problem):

    def __init__(self,capacity_V0):
        self.capacity_V0=capacity_V0
        
        #Cound number of time where the load is zero
        n_time=int(len(capacity_V0.loc[capacity_V0.Load > 0]))
        N_Chiller=2
        decition_set=0
        decition_set=N_Chiller-1 #load
        decition_set=decition_set+(2*N_Chiller) #Evap for each chiller and Flow for each chiller        
        Var_number=(n_time*decition_set)+(N_Chiller-1)
        
        XL=np.zeros((Var_number,), dtype=int)
        XU=np.ones((Var_number,), dtype=int)
        super().__init__(n_var=Var_number,
                         n_obj=2,
                         n_constr=4,
                         xl=np.array(XL),
                         xu=np.array(XU)
                        )

    def _evaluate(self, xx, out, *args, **kwargs):
        decition_set=5
        load_demand = self.capacity_V0['Load']
        amb = self.capacity_V0['Amb']
        ACC1_Flow = self.capacity_V0['ACC1_Flow'][0]
        ACC2_Flow = self.capacity_V0['ACC1_Flow'][0]
        ACC1_Flow_var = self.capacity_V0['ACC1_Flow_var'][0]
        ACC2_Flow_var = self.capacity_V0['ACC2_Flow_var'][0]
        ACC1_size = self.capacity_V0['ACC1_size'][0]
        ACC2_size = self.capacity_V0['ACC2_size'][0]
        CWST_Min = self.capacity_V0['CWST_Min'][0]
        CWST_Max = self.capacity_V0['CWST_Max'][0]

        # decition var------------------------------------
        X=np.zeros((xx.shape[0],24*decition_set))
        n_var=(xx.shape[1]-1)/decition_set
        active_acc1= 0 * X[:, 0]
        
        Max_Load=np.amax(load_demand)
        if Max_Load > ACC1_size and Max_Load > ACC2_size:
            active_acc1[:]=1
        else:
            active_acc1[:]=np.round(xx[:,-1])
            
        tt=0
        for t in range(0, 24):
            if self.capacity_V0['Load'][t]==0:
                X[:,t]=0
                X[:,t+24]=0
                X[:,t+48]=0
                X[:,t+72]=0
                X[:,t+96]=0
            else:
                X[:,t]=xx[:,tt]
                X[:,t+24]=xx[:,int(tt+(n_var*1))]
                X[:,t+48]=xx[:,int(tt+(n_var*2))]
                X[:,t+72]=xx[:,int(tt+(n_var*3))]
                X[:,t+96]=xx[:,int(tt+(n_var*4))]
                tt=tt+1
        
        # initialisation
        #print(X.shape)
        ACC1_EvapTout = 0 * X[:, 0:24]
        ACC2_EvapTout = 0 * X[:, 0:24]
        ACC1_dt_opt = 0 * X[:, 0:24]
        ACC2_dt_opt = 0 * X[:, 0:24]
        AccLoad_1 = 0 * X[:, 0:24]
        AccLoad_2 = 0 * X[:, 0:24]
        COP_1 = 0 * X[:, 0:24]
        COP_2 = 0 * X[:, 0:24]
        pump_1 = 0 * X[:, 0:24]
        pump_2 = 0 * X[:, 0:24]
        Flow_1 = 0 * X[:, 0:24]
        Flow_2 = 0 * X[:, 0:24]
        ACC1_start = 0 * X[:, 0]
        ACC2_start = 0 * X[:, 0]
        ACC1_power = 0 * X[:, 0:24]
        ACC2_power = 0 * X[:, 0:24]
        
        #if active_acc1=1, both active, if active_acc1=0 then 1 active
        load_demand=np.array(load_demand)
        AccLoad_1=load_demand*X[:,0:24]
        AccLoad_1=(AccLoad_1.T*active_acc1).T
        AccLoad_2=load_demand - AccLoad_1
        #for t in range(0, 24):
        #    AccLoad_1[:, t] = X[:, t]*active_acc1[:] * load_demand[t]
        #    AccLoad_2[:, t] =load_demand[t]-AccLoad_1[:, t]

        load2_1 = AccLoad_1 ** 2
        load3_1 = AccLoad_1 ** 3

        load2_2 = AccLoad_2 ** 2
        load3_2 = AccLoad_2 ** 3

        for t in range(0, 24):
            if load_demand[t] == 0:
                COP_1[:, t] = 0
                COP_2[:, t] = 0
                Flow_1[:, t] = 0
                Flow_2[:, t] = 0
                pump_1[:, t] = 0
                pump_2[:, t] = 0
                ACC1_EvapTout[:,t]=12
                ACC2_EvapTout[:,t]=12
                ACC1_dt_opt[:,t]=0
                ACC2_dt_opt[:,t]=0
                
            else:
                ACC1_EvapTout[:,t]=CWST_Min+((CWST_Max-CWST_Min)*X[:, t+24])
                ACC2_EvapTout[:,t]=CWST_Min+((CWST_Max-CWST_Min)*X[:, t+24+24])
                
                Flow_1[:,t]=(ACC1_Flow-ACC1_Flow_var)+((2*ACC1_Flow_var)*X[:, t+24+24+24])
                Flow_2[:,t]=(ACC2_Flow-ACC2_Flow_var)+((2*ACC2_Flow_var)*X[:, t+24+24+24+24])
            
                #ACC1_dt_opt[:,t]=2+((7-2)*X[:, t+24+24+24])
                #ACC2_dt_opt[:,t]=2+((7-2)*X[:, t+24+24+24+24])
            
                COP_1[:, t] = ((a1 * amb[t]) + (a2 * ACC1_EvapTout[:,t]) + (a3 * AccLoad_1[:, t]) + (a4 * load2_1[:, t]) + (a5 * load3_1[:, t]) + a6)
                COP_2[:, t] = ((a1 * amb[t]) + (a2 * ACC2_EvapTout[:,t]) + (a3 * AccLoad_2[:, t]) + (a4 * load2_2[:, t]) + (a5 * load3_2[:, t]) + a6)
                
                ACC1_dt_opt[:, t] =  AccLoad_1[:, t] / (4.18 * Flow_1[:,t])
                ACC2_dt_opt[:, t] = AccLoad_2[:, t] / (4.18 * Flow_2[:,t])

                pump_1[:, t] = Pump_ACC1_Power0 * (Flow_1[:, t] / Pump_ACC1_Flow0) ** 2
                pump_2[:, t] = Pump_ACC2_Power0 * (Flow_2[:, t] / Pump_ACC2_Flow0) ** 2
                
     
        #print(X.shape[0])
        for r in range(0, X.shape[0]):
            start_1 = 0
            start_2 = 0
            for t in range(0, 24 - 1):               
                if AccLoad_1[r,t] == 0:
                    COP_1[r, t] = 0
                    Flow_1[r, t] = 0
                    pump_1[r, t] = 0
                    ACC1_EvapTout[r,t]=12
                    ACC1_dt_opt[r,t]=0
                    
                if AccLoad_2[r,t] == 0:
                    COP_2[r, t] = 0
                    Flow_2[r, t] = 0
                    pump_2[r, t] = 0
                    ACC2_EvapTout[r,t]=12
                    ACC2_dt_opt[r,t]=0
                    
                if int(AccLoad_1[r, t]) == 0 and int(AccLoad_1[r, t + 1]) > 0:
                    start_1 = start_1 + 1
                if int(AccLoad_2[r, t]) == 0 and int(AccLoad_2[r, t + 1]) > 0:
                    start_2 = start_2 + 1
                    
            ACC1_start[r] = start_1
            ACC2_start[r] = start_2
        
        
        COP_1[COP_1 < 0] = 0
        COP_2[COP_2 < 0] = 0

        
        ACC1_power = np.nan_to_num(AccLoad_1/COP_1)
        ACC2_power = np.nan_to_num(AccLoad_2/COP_2)
        
    
        mindt_1=np.where(ACC1_dt_opt>0, ACC1_dt_opt, ACC1_dt_opt.max()).min(1) 
        mindt_2=np.where(ACC2_dt_opt>0, ACC2_dt_opt, ACC2_dt_opt.max()).min(1)
        
        minFlow_1=np.where(Flow_1>0, Flow_1, Flow_1.max()).min(1) 
        minFlow_2=np.where(Flow_2>0, Flow_2, Flow_2.max()).min(1)
        
        ACC1_MaxLoad=np.amax(AccLoad_1, axis=1) 
        ACC2_MaxLoad=np.amax(AccLoad_2, axis=1)

        
        f1 = (np.sum(pump_1, axis=1) + np.sum(pump_2, axis=1) + np.sum(ACC1_power, axis=1) + np.sum(ACC2_power, axis=1))
        f2 = 500*(ACC1_start + ACC2_start)
        #print(ACC1_start)
    
        
        g1 =  ACC1_start[:] - 1  # ax1+ax2<=0
        g2 =  ACC2_start[:] -1
        g3 = ACC1_MaxLoad[:]-ACC1_size
        g4 = ACC2_MaxLoad[:]-ACC2_size
        out["F"] = np.column_stack([f1, f2])
        out["G"] = np.column_stack([g1, g2, g3, g4])
