import os
import pickle

from google.auth.transport.requests import Request
from google.cloud import storage
import pandas as pd
import numpy
import gspread
from google.oauth2 import service_account
from config import *

#from utils import generate_tmp_path
#############################################################
#                    Get data from google sheet                #
#############################################################
def generate_df_from_googlesheet():
    import pandas as pd
    import google.auth
    import numpy as np
    import gspread
    from google.oauth2 import service_account
    import gspread_dataframe as gd
    # from oauth2client.client import GoogleCredentials
    credentials, project_id = google.auth.default(
        scopes=['https://www.googleapis.com/auth/cloud-platform',
            'https://www.googleapis.com/auth/drive',
            'https://www.googleapis.com/auth/spreadsheets'])

    gc = gspread.authorize(credentials)
    #from config import *

    # Connect to google spreadsheet
    # Define google parameters
    # Get load data
    ws = gc.open_by_key(SPREADSHEET_KEY).worksheet(INPUT_WORKSHEET_KEY)
    operator_input_df = pd.DataFrame(ws.get_all_records())
    ws2 = gc.open_by_key(SPREADSHEET_KEY).worksheet(INPUT_WORKSHEET_KEY_2)
    operator_input_df_2 = pd.DataFrame(ws2.get_all_records())
    #operator_input_df.to_csv('List_Data.csv',index=False)

    emptyVal = np.zeros(shape=(24,2))
    df= pd.DataFrame(emptyVal,columns=['Load','Amb'])
    df['Load']=operator_input_df['Load']
    df['Amb']=operator_input_df['Ambient temperature']
    df['ACC1_Flow']=operator_input_df_2['ACC1 Flow']
    df['ACC2_Flow']=operator_input_df_2['ACC2 Flow']
    df['ACC1_Flow_var']=operator_input_df_2['variation Flow ACC1']
    df['ACC2_Flow_var']=operator_input_df_2['variation Flow ACC2']
    df['ACC1_size']=operator_input_df_2['ACC1 size']
    df['ACC2_size']=operator_input_df_2['ACC2 size']
    df['CWST_Min']=operator_input_df_2['CWST Min']
    df['CWST_Max']=operator_input_df_2['CWST Max']
    df=df.reset_index(drop=True)
    return df
    
    
# prepare the results #######################################
#############################################################
def Prepare_results(capacity_V0,res):
    import pandas as pd
    import numpy as np
    import pickle
    columns=['Load','Amb','ACC1_dt_opt','ACC2_dt_opt','x',
            'ACC1 EvapTout','ACC2 EvapTout',
            'ACC1 Load','ACC2 Load',
             'ACC1 COP','ACC2 COP',
             'ACC1 Power [kW]','ACC2 Power [kW]',
             'ACC1&2 Power [kW]',
             'ACC1&2 COP',
             'ACC1 Flow','ACC2 Flow',
             'Pump1 Power [kW]','Pump2 Power [kW]',
             'Pump1&2 Power [kW]',
             'Overal Power [kW]',
             'Overal COP']
    # decition var------------------------------------
    ACC1_size = capacity_V0['ACC1_size'][0]
    ACC2_size = capacity_V0['ACC2_size'][0]
    decition_set=5
    n_var=int(len(capacity_V0.loc[capacity_V0.Load > 0]))
    X=np.zeros((1,24*decition_set))
    if res.X is None:
        for t in range(0, 24):
            if capacity_V0['Load'][t]==0:
                X[0,t]=0
                X[0,t+24]=0
                X[0,t+48]=0
                X[0,t+72]=0
                X[0,t+96]=0
            else:
                if ACC1_size==0:
                    X[0,t]=0
                else:
                    X[0,t]=1 
                X[0,t+24]=1
                X[0,t+48]=1
                X[0,t+72]=0
                X[0,t+96]=0
    else:
        if len(res.X) > 1:
            xx=res.X[0,:]
        else:
            xx=res.X[0,:]
        tt=0
        for t in range(0, 24):
            if capacity_V0['Load'][t]==0:
                X[0,t]=0
                X[0,t+24]=0
                X[0,t+48]=0
                X[0,t+72]=0
                X[0,t+96]=0
            else:
                X[0,t]=xx[tt]
                X[0,t+24]=xx[int(tt+(n_var*1))]
                X[0,t+48]=xx[int(tt+(n_var*2))]
                X[0,t+72]=xx[int(tt+(n_var*3))]
                X[0,t+96]=xx[int(tt+(n_var*4))]
                tt=tt+1
    Max_Load=np.amax(capacity_V0['Load'])
    if Max_Load > ACC1_size and Max_Load > ACC2_size:
        active_acc1=1
    else:
        active_acc1=np.round(xx[-1])
        
    X_zero=np.zeros((24, 22))
    capacity_V=pd.DataFrame(X_zero,columns=columns) 
    
    ACC1_Flow = capacity_V0['ACC1_Flow'][0]
    ACC2_Flow = capacity_V0['ACC1_Flow'][0]
    ACC1_Flow_var = capacity_V0['ACC1_Flow_var'][0]
    ACC2_Flow_var = capacity_V0['ACC2_Flow_var'][0]
    CWST_Min = capacity_V0['CWST_Min'][0]
    CWST_Max =capacity_V0['CWST_Max'][0]
        
    capacity_V['Load']=capacity_V0[['Load']].copy()
    capacity_V['Amb']=capacity_V0[['Amb']].copy()
    capacity_V['x']=X[0,0:24]

            
    capacity_V['ACC1 EvapTout']=CWST_Min+((CWST_Max-CWST_Min)*X[0,24:48])
    capacity_V['ACC2 EvapTout'] = CWST_Min + ((CWST_Max - CWST_Min) * X[0,48:72])
    
    capacity_V['ACC1 Flow']=(ACC1_Flow-ACC1_Flow_var)+((2*ACC1_Flow_var)*X[0,72:96])
    capacity_V['ACC2 Flow']=(ACC2_Flow-ACC2_Flow_var)+((2*ACC2_Flow_var)*X[0,96:120])

    capacity_V['ACC1 Load']=capacity_V['x']*active_acc1*capacity_V0['Load']
    capacity_V['ACC2 Load']=capacity_V0['Load']-capacity_V['ACC1 Load']
    
    #capacity_V['ACC1 Load']=1*capacity_V0['Load']
    #capacity_V['ACC2 Load']=0*capacity_V0['Load']

    capacity_V['ACC1 COP']=((a1 * capacity_V['Amb']) + (a2 * capacity_V['ACC1 EvapTout']) + (a3 * capacity_V['ACC1 Load']) + (a4 * (capacity_V['ACC1 Load']**2)) + (a5 * (capacity_V['ACC1 Load']**3)) + a6)
    capacity_V['ACC2 COP']=((a1 * capacity_V['Amb']) + (a2 * capacity_V['ACC2 EvapTout']) + (a3 * capacity_V['ACC2 Load']) + (a4 * (capacity_V['ACC2 Load']**2)) + (a5 * (capacity_V['ACC2 Load']**3)) + a6)
    
    capacity_V['Pump1 Power [kW]']=Pump_ACC1_Power0*(capacity_V['ACC1 Flow']/Pump_ACC1_Flow0)**2
    capacity_V['Pump2 Power [kW]']=Pump_ACC2_Power0*(capacity_V['ACC2 Flow']/Pump_ACC2_Flow0)**2
    
    capacity_V['ACC1 Power [kW]']=capacity_V['ACC1 Load'].div(capacity_V['ACC1 COP']).replace(np.inf,0)
    capacity_V['ACC2 Power [kW]']=capacity_V['ACC2 Load'].div(capacity_V['ACC2 COP']).replace(np.inf, 0)
    capacity_V.fillna(0, inplace=True)
    
    
    for t in range(0, 24):
        if capacity_V0['Load'][t] == 0:
            capacity_V['ACC1 COP'][t] = 0
            capacity_V['ACC2 COP'][t] = 0
            capacity_V['ACC1 Flow'][t]=0
            capacity_V['ACC2 Flow'][t]=0
            capacity_V['Pump1 Power [kW]'][t]=0
            capacity_V['Pump2 Power [kW]'][t]=0
            capacity_V['ACC1 EvapTout'][t]=12
            capacity_V['ACC2 EvapTout'][t]= 12
            capacity_V['ACC1_dt_opt'][t]=0
            capacity_V['ACC2_dt_opt'][t]=0
            capacity_V['ACC1 Power [kW]'][t]=0
            capacity_V['ACC2 Power [kW]'][t]=0
        else:
            capacity_V['ACC1_dt_opt'][t]=capacity_V['ACC1 Load'][t]/(4.18*capacity_V['ACC1 Flow'][t])
            capacity_V['ACC2_dt_opt'][t]=capacity_V['ACC2 Load'][t]/(4.18*capacity_V['ACC2 Flow'][t])
        
        if capacity_V['ACC1 Load'][t] == 0:
            capacity_V['ACC1 COP'][t] = 0
            capacity_V['ACC1 Flow'][t]=0
            capacity_V['Pump1 Power [kW]'][t]=0
            capacity_V['ACC1 EvapTout'][t]=12
            capacity_V['ACC1_dt_opt'][t]=0
            capacity_V['ACC1 Power [kW]'][t]=0
       
        if capacity_V['ACC2 Load'][t] == 0:
            capacity_V['ACC2 COP'][t] = 0
            capacity_V['ACC2 Flow'][t]=0
            capacity_V['Pump2 Power [kW]'][t]=0
            capacity_V['ACC2 EvapTout'][t]=12
            capacity_V['ACC2_dt_opt'][t]=0
            capacity_V['ACC2 Power [kW]'][t]=0

            

    capacity_V['ACC1&2 Power [kW]']=capacity_V['ACC1 Power [kW]']+capacity_V['ACC2 Power [kW]']
    capacity_V['ACC1&2 COP']=capacity_V['Load'].div(capacity_V['ACC1&2 Power [kW]']).replace([np.inf, -np.inf],0)
    capacity_V.fillna(0, inplace=True)

    #Flow
    #capacity_V['ACC1_dt_opt']=capacity_V['ACC1 Load'].div((4.18*capacity_V['ACC1 Flow'])).replace(np.inf, 0)
    #capacity_V['ACC2_dt_opt']=capacity_V['ACC2 Load'].div((4.18*capacity_V['ACC2 Flow'])).replace(np.inf, 0)
    #capacity_V.fillna(0, inplace=True)

    capacity_V['Pump1&2 Power [kW]'] = capacity_V['Pump1 Power [kW]'] + capacity_V['Pump2 Power [kW]']
    capacity_V['Overal Power [kW]']=capacity_V['Pump1 Power [kW]'] + capacity_V['Pump2 Power [kW]']+capacity_V['ACC1 Power [kW]']+capacity_V['ACC2 Power [kW]']
    capacity_V['Overal COP'] = capacity_V['Load'].div(capacity_V['Overal Power [kW]']).replace([np.inf, -np.inf], 0)
    capacity_V.fillna(0, inplace=True)
    capacity_V=capacity_V.drop(['x'], axis = 1)
    #print(np.sum(capacity_V['Overal Power [kW]']))
    return capacity_V

    


def main_export_results(capacity_V):
#############################################################
#                    Export data to google sheet            #
#############################################################
# install dependencies
    # %pip install --upgrade gspread
    # %pip install --upgrade gspread-dataframe
# make sure the service account has rw access to the google spreadsheets.
    import gspread
    import gspread_dataframe as gd
    import google.auth

    SPREADSHEET_KEY = "1sqkrJnZ6xE1KwZFCs9uBZ42XKqN4wCDOmsaAviW62BE"
    INPUT_WORKSHEET_KEY = "Cooling load demand"
    RESULTS_WORKSHEET_KEY = "Staging results"
    
    # from oauth2client.client import GoogleCredentials
    credentials, project_id = google.auth.default(
        scopes=['https://www.googleapis.com/auth/cloud-platform',
            'https://www.googleapis.com/auth/drive',
            'https://www.googleapis.com/auth/spreadsheets'])

    gc = gspread.authorize(credentials)

    def InsertInGsheet(df, spreadsheetId, sheetName, append):
        ws = gc.open_by_key(spreadsheetId).worksheet(sheetName)
        if append == True:
            df_rows = len(df.index)
            ws.add_rows(df_rows)
            gd.set_with_dataframe(worksheet=ws, dataframe=df,row=ws.row_count+1, col=1,include_column_header=False)
        else:
            ws.clear()
            gd.set_with_dataframe(worksheet=ws, dataframe=df, row=1, col=1)

    # give accesse to the foolowing service
    #HVAC_Staging_OPT@hk-ops-corp-operationdashboard.iam.gserviceaccount.com

    InsertInGsheet(capacity_V, SPREADSHEET_KEY, RESULTS_WORKSHEET_KEY, append=False)

    print("Results updated in Google Sheets") 




def generate_df_from_bigquery(date_from,date_to):
    #-------------------------------------------------
    #           Get data from Data Lake              #
    #-------------------------------------------------
    import google.auth
    from google.cloud import bigquery
    from google.cloud import bigquery_storage_v1
    import pandas

    credentials, project_id = google.auth.default(
        scopes=['https://www.googleapis.com/auth/cloud-platform',
                'https://www.googleapis.com/auth/bigquery',
                'https://www.googleapis.com/auth/drive',
                'https://www.googleapis.com/auth/spreadsheets']
    )

    client = bigquery.Client(
        credentials=credentials,
        project=project_id,
    )

    query = """
    SELECT
        CAST(DATETIME(timestamp_local, "+0:00") AS DATETIME) as measure_time,
        MAX(IF(sensor_name = "Outdoor Temperature Sensor oC",value,NULL)) as ACC1_Amb,
        AVG(IF(sensor_name = "ACC1 Chilled Water Flowrate ls" ,value,NULL)) as ACC1_Chilled_Water_Flowrate,
        MAX(IF(sensor_name = "ACC1 Chilled Water Return Temperature oC" ,value,NULL)) as ACC1_Chilled_Water_Return_Temperature_oC,
        MAX(IF(sensor_name = "ACC1 Chilled Water Supply Temperature oC" ,value,NULL)) as ACC1_EvapTout,
        SUM(IF(sensor_name = "ACC1 Cooling Production EM kWh",value,NULL)) as ACC1_CoolingLoad_kWh,  
        SUM(IF(sensor_name = "ACC1 Energy Consumption kwh",value,NULL)) as ACC1_Power_kWh,
        MAX(IF(sensor_name = "Outdoor Temperature Sensor oC",value,NULL)) as ACC2_Amb,
        AVG(IF(sensor_name = "ACC2 Chilled Water Flowrate ls" ,value,NULL)) as ACC2_Chilled_Water_Flowrate,
        MAX(IF(sensor_name = "ACC2 Chilled Water Return Temperature oC",value,NULL)) as ACC2_Chilled_Water_Return_Temperature_oC,
        MAX(IF(sensor_name = "ACC2 Chilled Water Supply Temperature oC",value,NULL)) as ACC2_EvapTout,
        MAX(IF(sensor_name = "ACC2 Cooling Production EM kWh",value,NULL)) as ACC2_CoolingLoad_kWh,    
        MAX(IF(sensor_name = "ACC2 Energy Consumption kwh",value,NULL)) as ACC2_Power_kWh
      FROM
        `hk-ops-corp-operationdashboard.projects_data.bsds_fo_measure_v2_sunhouse`
      WHERE timestamp_local >= @DF
        AND timestamp_local <= @DT 
        AND timeserie_type ="CONVERTED"
        AND sensor_name IN (
            "Outdoor Temperature Sensor oC",
            "ACC1 Chilled Water Flowrate ls",
            "ACC1 Chilled Water Return Temperature oC",
            "ACC1 Chilled Water Supply Temperature oC",
            "ACC1 Cooling Production EM kWh",
            "ACC1 Energy Consumption kwh",
            "Outdoor Temperature Sensor oC",
            "ACC2 Chilled Water Flowrate ls",
            "ACC2 Chilled Water Return Temperature oC",
            "ACC2 Chilled Water Supply Temperature oC",
            "ACC2 Cooling Production EM kWh",
            "ACC2 Energy Consumption kwh"
        )

      GROUP BY timestamp_local
      ORDER BY timestamp_local desc
    """

    # Define the parameter values in a query job configuration
    date_to=date_to+' 23:59:59'
    job_config = bigquery.QueryJobConfig(
        query_parameters=[
            bigquery.ScalarQueryParameter("DF", "STRING", date_from),
            bigquery.ScalarQueryParameter("DT", "STRING", date_to),
        ]
    )

    query_job = client.query(query, job_config=job_config)    
    df = query_job.to_dataframe()
    df = df.sort_values(by=["measure_time"])
    df=df.reset_index(drop=True)

    return df
